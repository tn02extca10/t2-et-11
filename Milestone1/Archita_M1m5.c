 #include <stdio.h>
  #include <math.h>
  int main () {
        float s, l1, l2, l3, a;

        printf("Enter ur inputs for 3 sides:\n");
        scanf("%f%f%f", &l1, &l2, &l3);
        s = (l1 + l2 + l3) / 2;
        
        a = sqrt(s * (s - l1) * (s - l2) * (s - l3));
		printf("Area of triangle: %f\n", a);
        return 0;
  }