#include<stdio.h>
#include<math.h>

int factor(int n);

int main ()
{
	int n;
	printf("Enter the number: ");
	scanf("%d", &n);
	
	factor(n);
}

int factor(int n)
{
	int i;
	printf("Factors of the given number (%d):\n",n);
	for(i=1; i <= n; ++i)
    {
        if (n%i == 0)
        {
			printf("%d  ",i);
        }
    }
	printf("\n");
}