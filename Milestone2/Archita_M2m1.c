#include <stdio.h>
#include <math.h>
int main()
{
    double x, result;
	double n;
    printf("Enter a base number: ");
    scanf("%lf", &x);
    printf("Enter an exponent: ");
    scanf("%lf", &n);
    result = pow(x,n);
    printf("%.1lf^%.llf = %.2lf", x, n, result);
    return 0;
}