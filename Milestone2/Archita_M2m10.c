#include<stdio.h>
 
int fact(int);
 
main()
{
   int number;
   int f = 1;
 
   printf("Enter a number to calculate it's factorial\n");
   scanf("%d",&number);
 
   printf("%d! = %ld\n", number, fact(number));
 
   return 0;
}
 
int fact(int n)
{
   int c;
   int result = 1;
 
   for( c = 1 ; c <= n ; c++ )
         result = result*c;
 
   return ( result );
}