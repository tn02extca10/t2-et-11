
#include<stdio.h>
#include<math.h>

int array[50];

void main ()
{
	int n,t,f=0;
	
	printf("Enter the number of elements in array[up to 50]: ");
	scanf("%d", &n);
	
	printf("Enter the %d elements of array:\n",n);
	for(int i=0; i<n; i++)
	{
		scanf("%d", &array[i]);
	}
	
	printf("Enter element to search: ");
	scanf("%d", &t);
	
	for(int i=0; i<n; i++)
	{
		if(array[i] == t)
		{
			f = 1;
			break;
		}
	}
	if(f==1)
	{
		printf("%d is found in the array.\n",t);
	}
	else
	{
		printf("%d is not found in the array.\n",t);
	}
}

